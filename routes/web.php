<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
});
 */
Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

/*
|--------------------------------------------------------------------------
| Student Routes
|--------------------------------------------------------------------------
*/
Route::get('/students/show_deleted', [App\Http\Controllers\StudentController::class, 'show_deleted'])->name('students.show_deleted');
Route::put('/students/restore/{student}', [App\Http\Controllers\StudentController::class, 'restore'])->name('students.restore')->withTrashed();
Route::resource('/students', App\Http\Controllers\StudentController::class);

/*
|--------------------------------------------------------------------------
| Learnday Routes
|--------------------------------------------------------------------------
*/
Route::get('/learndays/show_deleted', [App\Http\Controllers\LearndayController::class, 'show_deleted'])->name('learndays.show_deleted');
Route::get('/learndays/manage/{learnday}', [App\Http\Controllers\LearndayController::class, 'delete_index'])->name('learndays.delete_attendances');
Route::put('/learndays/restore/{learnday}', [App\Http\Controllers\LearndayController::class, 'restore'])->name('learndays.restore')->withTrashed();
Route::resource('/learndays', App\Http\Controllers\LearndayController::class);

/*
|--------------------------------------------------------------------------
| Attendance Routes
|--------------------------------------------------------------------------
*/

Route::get('/attendances/show_deleted', [App\Http\Controllers\AttendanceController::class, 'show_deleted'])->name('attendances.show_deleted');
Route::get('/attendances/populate_attendance', [App\Http\Controllers\AttendanceController::class, 'populateAttendance'])->name('attendances.populate_attendance');
Route::put('/attendances/mass_update/{learnday}', [App\Http\Controllers\AttendanceController::class, 'massUpdate'])->name('attendances.mass_update');
Route::put('/attendances/restore/{attendance}', [App\Http\Controllers\AttendanceController::class, 'restore'])->name('attendances.restore')->withTrashed();
Route::resource('/attendances', App\Http\Controllers\AttendanceController::class);