<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Attendance>
 */
class AttendanceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'student_id' => fake()->numberBetween(1,32),
            'learnday_id' => fake()->numberBetween(1,4),
            'status' => fake()->randomElement(['jelen', 'hiányzó', 'keresőképtelen'])
        ];
    }
}
