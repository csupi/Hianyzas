<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LearndaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i=1; $i < 4; $i++) { 
            \App\Models\Learnday::factory()->create([
                'name' => 'Title1',
                'date' => '2024-01-22',
                'course_id' => $i,
            ]);
    
            \App\Models\Learnday::factory()->create([
                'name' => 'Title2',
                'date' => '2024-01-23',
                'course_id' => $i,
            ]);
    
            \App\Models\Learnday::factory()->create([
                'name' => 'Title3',
                'date' => '2024-01-24',
                'course_id' => $i,
            ]);
    
            \App\Models\Learnday::factory()->create([
                'name' => 'Title4',
                'date' => '2024-01-25',
                'course_id' => $i,
            ]);
        }
    }
}
