<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /* \App\Models\User::factory()->create([
            'id' => 0,
            'name' => 'Error User',
            'email' => 'error@example.com',
            'password' => '12345',
            'role_id' => '1',
       ]); */

        \App\Models\User::factory()->create([
            'name' => 'Admin User',
            'email' => 'test@example.com',
            'password' => '12345',
            'role_id' => '1',
        ]);

        \App\Models\User::factory()->create([
            'name' => 'Manager User',
            'email' => 'manager@example.com',
            'password' => '12345',
            'role_id' => '2',
        ]);
    }
}
