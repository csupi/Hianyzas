<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        $this->call([
            UserSeeder::class,
            CourseSeeder::class,
            StudentSeeder::class,
            LearndaySeeder::class,
            AttendanceSeeder::class,
        ]);


        // Filling up attendances table when seeding
        \App\Models\Learnday::all()->each(
            function($learnday)
            {
                $course = $learnday->course;
                $course_students = $course->students()->get();

                foreach ($course_students as $student) {
                    $student->learndays()->attach($learnday->id, ['status' => 'jelen']);
                }

            }
        );
    }
}
