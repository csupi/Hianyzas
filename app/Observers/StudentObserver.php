<?php

namespace App\Observers;

use App\Models\Student;

class StudentObserver
{
    /**
     * Handle the Student "created" event.
     */
    public function creating(Student $student): void
    {
        $user = auth()->user();
        $student->created_by = $user ? $user->id : null;
    }

    /**
     * Handle the Student "updated" event.
     */
    public function updating(Student $student): void
    {
        $user = auth()->user();
        $student->updated_by = $user ? $user->id : null;
    }

    /**
     * Handle the Student "deleted" event.
     */
    public function deleted(Student $student): void
    {
        $user = auth()->user();
        $student->deleted_by = $user ? $user->id : null;
        $student->save();
    }

    /**
     * Handle the Student "restored" event.
     */
    public function restored(Student $student): void
    {
        //
    }

    /**
     * Handle the Student "force deleted" event.
     */
    public function forceDeleted(Student $student): void
    {
        //
    }
}
