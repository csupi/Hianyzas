<?php

namespace App\Observers;

use App\Models\Attendance;

class AttendanceObserver
{
    /**
     * Handle the Attendance "created" event.
     */
    public function creating(Attendance $attendance): void
    {
        $user = auth()->user();
        $attendance->created_by = $user ? $user->id : null;
    }

    /**
     * Handle the Attendance "updated" event.
     */
    public function updating(Attendance $attendance): void
    {
        $user = auth()->user();
        $attendance->updated_by = $user ? $user->id : null;
    }

    /**
     * Handle the Attendance "deleted" event.
     */
    public function deleted(Attendance $attendance): void
    {
        $user = auth()->user();
        $attendance->deleted_by = $user ? $user->id : null;
        $attendance->save();
    }

    /**
     * Handle the Attendance "restored" event.
     */
    public function restored(Attendance $attendance): void
    {
        //
    }

    /**
     * Handle the Attendance "force deleted" event.
     */
    public function forceDeleted(Attendance $attendance): void
    {
        //
    }
}
