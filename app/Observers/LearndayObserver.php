<?php

namespace App\Observers;

use App\Models\Learnday;

class LearndayObserver
{
    /**
     * Handle the Learnday "created" event.
     */
    public function creating(Learnday $learnday): void
    {
        $user = auth()->user();
        $learnday->created_by = $user ? $user->id : null;
    }

    /**
     * Handle the Learnday "updated" event.
     */
    public function updating(Learnday $learnday): void
    {
        $user = auth()->user();
        $learnday->updated_by = $user ? $user->id : null;
    }

    /**
     * Handle the Learnday "deleted" event.
     */
    public function deleted(Learnday $learnday): void
    {
        $user = auth()->user();
        $learnday->deleted_by = $user ? $user->id : null;
        $learnday->save();
    }

    /**
     * Handle the Learnday "restored" event.
     */
    public function restored(Learnday $learnday): void
    {
        //
    }

    /**
     * Handle the Learnday "force deleted" event.
     */
    public function forceDeleted(Learnday $learnday): void
    {
        //
    }
}
