<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Student;
use App\Http\Requests\StoreStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $this->authorize('viewAny', Student::class);

        $students = Student::all();
        return view('students.index', ['students' => $students]);
    }
    
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->authorize('create', Student::class);

        $courses = Course::all();
        return view('students.create', ['courses' => $courses]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreStudentRequest $request)
    {
        $this->authorize('create', Student::class);

        
        $Student = Student::create($request->all());
        $Student->save();

        // Create attendances to the course's learning days
        $course = Course::find($request->course_id);
        $courseLearndays = $course->learningdays()->get();
        
        foreach ($courseLearndays as $key => $learnday) {
            $Student->learndays()->attach($learnday->id, ['status' => 'jelen']);
        }

        return back()->with("success","Student created successfully.");
    }

    /**
     * Display the specified resource.
     */
    public function show(Student $student)
    {
        $this->authorize('view', $student);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Student $student)
    {
        $this->authorize('update', $student);

        $courses = Course::all();
        return view('students.edit', ['student' => $student, 'courses' => $courses]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateStudentRequest $request, Student $student)
    {
        $this->authorize('update', $student);

        // Get the new and old course
        $oldCourse = $student->course;
        $newCourse = Course::find(intval($request->course_id));

        // Get new and old course's learndays
        $oldCourseLearningDays = $oldCourse->learningdays()->get();
        $newCourseLearningDays = $newCourse->learningdays()->get();

        if ($oldCourse != $newCourse)
        {
            foreach ($oldCourseLearningDays as $key => $learnday) {

                // Force delete all attendances that connected to the old course's learning days
                $attendances = $learnday->attendances->where('student_id', $student->id);

                foreach ($attendances as $key => $attendance) {
                    $attendance->delete();
                    \App\Models\Attendance::withTrashed()->where('id', $attendance->id)->forceDelete();
                }
            }

            foreach ($newCourseLearningDays as $key => $learnday) {
                $student->learndays()->attach($learnday->id, ['status' => 'jelen']);
            }

        }

        $student->update($request->all());
        return redirect()->route('students.index')->with( 'success','The student was updated successfully.' );
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Student $student)
    {
        $this->authorize('delete', $student);

        foreach ($student->attendances as $key => $attendance) {
            $attendance->delete();
        }

        $student->delete();
        return back()->with('success','Successfully deleted the student: '.$student->name.'.');
    }


    public function show_deleted(){
        $this->authorize('viewDeleted', Student::class);

        $deleted_students = Student::onlyTrashed()->get();
        return view('students.show_deleted', ['deleted_students'=> $deleted_students]);
    }

    public function restore(Student $student)
    {
        $this->authorize('restore', $student);

        foreach ($student->attendances()->withTrashed()->get() as $key => $deleted_attendance) {
            $deleted_attendance->restore();
        }

        $student->restore();
        return back()->with('success',''.$student->name.' was successfully restored.');
    }
}
