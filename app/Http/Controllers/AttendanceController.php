<?php

namespace App\Http\Controllers;

use App\Models\Attendance;
use App\Http\Requests\StoreAttendanceRequest;
use App\Http\Requests\UpdateAttendanceRequest;
use App\Models\Learnday;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreAttendanceRequest $request)
    {

    }

    /**
     * Display the specified resource.
     */
    public function show(Attendance $attendance)
    {
        return view('attendances.show', ['attendance' => $attendance]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Attendance $attendance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateAttendanceRequest $request, Attendance $attendance)
    {
        /* dd($request); */
    }

    public function massUpdate(Request $request, Learnday $learnday)
    {
        $students = collect();
        $studentsNew = $learnday->attendances->where('deleted_at', null); // Get not deleted attendances
        
        // Filling up the collection with the students
        foreach ($studentsNew as $key => $value) {
            $students->push($value->student);
        }
        
        foreach ($students as $key => $student) {
            $studentStatus = $student->attendances->where('learnday_id', $learnday->id)->first()->status; // Student's current status

            // Validating user status exists from input request
            if ( array_key_exists('status_'.$student->id, $request->all() )  ) {
                $studentInputStatus = $request->all()['status_'.$student->id]; // Student's current status from input

                if ($studentStatus != $studentInputStatus) 
                {                    
                    // Updating attendance
                    Attendance::where('student_id', $student->id)->where('learnday_id', $learnday->id)->update([
                        'status' => $studentInputStatus,
                    ]);
                }
            }
        }
        return back()->with('success', 'Successfully saved the changes.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Attendance $attendance)
    {
        $attendance->delete();
        return back()->with('success','Successfully deleted the attendance');
    }

    public function populateAttendance(Request $request)
    {
        $learnday = Learnday::find($request->learnday_id);
        $course = \App\Models\Course::find($request->course_id);
        $course_students = $course->students()->get();

        foreach ($course_students as $student) {
            $student->learndays()->attach($learnday->id, ['status' => 'jelen']);
        }

        return redirect()->route('learndays.index');
    }

    public function show_deleted(){
        $deleted_attendances = Attendance::onlyTrashed()->get();
        return view('attendances.show_deleted', ['deleted_attendances'=> $deleted_attendances]);
    }

    public function restore(Attendance $attendance)
    {
        $attendance->restore();
        return back()->with('success',''.$attendance->student->name.' at '. $attendance->student->course->name .' on '. $attendance->learnday->date .' was successfully restored.');
    }
}
