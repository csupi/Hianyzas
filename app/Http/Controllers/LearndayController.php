<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Learnday;
use App\Http\Requests\StoreLearndayRequest;
use App\Http\Requests\UpdateLearndayRequest;
use Illuminate\Support\Facades\Auth;

class LearndayController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $this->authorize('viewAny', Learnday::class);

        $learndays = Learnday::all();
        return view('learndays.index', ['learndays' => $learndays]);
    }

    public function delete_index(Learnday $learnday)
    {
        $this->authorize('delete', $learnday);

        return view('learndays.delete_index', ['learnday' => $learnday]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->authorize('create', Course::class);

        $courses = Course::all();
        return view('learndays.create', ['courses' => $courses]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreLearndayRequest $request)
    {
        $this->authorize('create', Course::class);

        $learnday = Learnday::create($request->all());
        $learnday->save();

        return redirect()->route('attendances.populate_attendance', ['learnday_id' => $learnday->id, 'course_id' => $learnday->course_id]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Learnday $learnday)
    {
        $this->authorize('view', $learnday);

        return view('learndays.show', ['learnday' => $learnday]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Learnday $learnday)
    {
        $this->authorize('update', $learnday);

        return view('learndays.edit', ['learnday' => $learnday]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateLearndayRequest $request, Learnday $learnday)
    {
        $this->authorize('update', $learnday);

        $learnday->update($request->all());
        return redirect()->route('learndays.index')->with('success', 'The learnday was updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Learnday $learnday)
    {
        $this->authorize('delete', $learnday);

        foreach ($learnday->attendances()->get() as $key => $attendance) {
            $attendance->delete();
        }

        $learnday->delete();
        return back()->with('success', 'Successfully deleted the learnday: ' . $learnday->name . '.');
    }

    public function show_deleted()
    {
        $this->authorize('viewDeleted', Learnday::class);

        $deleted_learndays = Learnday::onlyTrashed()->get();
        return view('learndays.show_deleted', ['deleted_learndays' => $deleted_learndays]);
    }

    public function restore(Learnday $learnday)
    {
        $this->authorize('restore', $learnday);

        foreach ($learnday->attendances()->withTrashed()->get() as $key => $attendance) {
            $attendance->restore();
        }

        $learnday->restore();
        return back()->with('success', '' . $learnday->name . ' was successfully restored.');
    }

}
