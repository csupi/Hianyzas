<?php

namespace App\Policies;

use App\Models\Learnday;
use App\Models\User;
use Illuminate\Auth\Access\Response;

class LearndayPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        if ($user->role_id === 1 || $user->role_id === 2) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Learnday $learnday): bool
    {
        if ($user->role_id === 1 || $user->role_id === 2) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can view the deleted models.
     */
    public function viewDeleted(User $user): bool
    {
        if ($user->role_id === 1) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        if ($user->role_id === 1) {
            return true;
        }

        return true;
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Learnday $learnday): bool
    {
        if ($user->role_id === 1 || $user->role_id === 2) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Learnday $learnday): bool
    {
        /* dd($user->role_id); */
        if ($user->role_id === 1) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, Learnday $learnday): bool
    {
        if ($user->role_id === 1) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(User $user, Learnday $learnday): bool
    {
        if ($user->role_id === 1) {
            return true;
        }

        return false;
    }
}
