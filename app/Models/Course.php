<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function students(): HasMany
    {
        return $this->hasMany(Student::class);
    }

    public function learningdays(): HasMany
    {
        return $this->hasMany(Learnday::class);
    }
}
