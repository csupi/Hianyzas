<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use App\Models\Attendance;
use App\Models\Student;
use App\Policies\AttendancePolicy;
use App\Policies\LearndayPolicy;
use App\Policies\StudentPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        Learnday::class => LearndayPolicy::class,
        Student::class => StudentPolicy::class,
        Attendance::class => AttendancePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        //
    }
}
