@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>

                        <strong>Holy guacamole!</strong>
                        <p>{{ Session::get('success') }}</p>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="mb-3 mt-3">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <strong>Holy guacamole!</strong>

                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif

                <table id="deletedAttendancesTable" class="display">
                    <thead>
                        <tr>
                            <th>Student name</th>
                            <th>Student birthdate</th>
                            <th>Learnday name</th>
                            <th>Learnday date</th>
                            <th>Learnday course</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($deleted_attendances as $item)
                            <tr>
                                <td>{{ $item->student->name }}</td>
                                <td>{{ $item->student->birthdate }}</td>
                                <td>{{ $item->learnday->name }}</td>
                                <td>{{ $item->learnday->date }}</td>
                                <td>{{ $item->learnday->course->name }}</td>
                                <td>
                                    <form action="{{ route('attendances.restore', ['attendance' => $item]) }}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <button type="submit" class="btn btn-info">Restore</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        let table = new DataTable('#deletedAttendancesTable', {
            responsive: true,
            ordering: true
        });
    </script>
@endsection
