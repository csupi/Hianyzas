@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>

                        <strong>Holy guacamole!</strong>
                        <p>{{ Session::get('success') }}</p>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="mb-3 mt-3">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <strong>Holy guacamole!</strong>

                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-center">Currently modifying: {{ $learnday->name }}</h4>
                        <form action={{ route('learndays.update', $learnday) }} method="POST">
                            @csrf
                            @method('PUT')
                            <div class="mb-3">
                                <label for="name" class="form-label">Name</label>
                                <input type="text" class="form-control" id="name" name="name"
                                    aria-describedby="nameHelp" value="{{ old('name', $learnday->name) }}">
                                <div id="nameHelp" class="form-text">Learnday's name</div>
                            </div>

                            <div class="mb-3">
                                <label for="date" class="form-label">Date</label>
                                <input type="date" class="form-control" id="date" name="date"
                                    aria-describedby="dateHelp" value="{{ old('date', $learnday->date) }}">
                                <div id="dateHelp" class="form-text">Learnday's date</div>
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-warning text-center">Modify</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="col-2"></div>
        </div>
    </div>
@endsection
