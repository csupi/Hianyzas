@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card bg-white">
                    <div class="card-body">
                        <div class="d-flex mb-3">
                            <h4 class="card-title">{{ $learnday->name }} - {{ $learnday->course->name }}</h4>
                            <a name="returnBack" id="returnBack" class="btn btn-success text-end" style="margin-left:auto !important" href="{{ route('learndays.show', ['learnday' => $learnday]) }}" role="button">Return back</a>
                        </div>

                        @if (Session::has('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="Close"></button>

                                <strong>Holy guacamole!</strong>
                                <p>{{ Session::get('success') }}</p>
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="mb-3 mt-3">
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                    <strong>Holy guacamole!</strong>

                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif

                        <div class="table-responsive">
                            <table class="table table-light">
                                <thead>
                                    <tr>
                                        <th scope="col">Name</th>
                                        <th scope="col" class="text-left">Birthday</th>
                                        <th scope="col" class="text-center">Status</th>
                                        <th scope="col" class="text-end">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($learnday->attendances as $item)
                                        {{-- {{ dd($item->student) }} --}}
                                        <tr class="">
                                            <td scope="row">{{ $item->student->name }}</td>
                                            <td class="text-left">{{ $item->student->birthdate }}</td>
                                            <td class="text-center">{{ $item->status }}</td>
                                            <td class="text-end">
                                                <form action="{{ route('attendances.destroy', $item) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </form>
                                            </td>
                                            {{-- <td class="text-end"> --}}
                                            {{-- <form action="{{ route('attendances.destroy', $item) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                    </form> --}}
                                            {{-- </td> --}}
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        {{-- <div class="text-end">
                            <button type="button" class="btn btn-primary text-end" onclick="callFormSubmit();">
                                Save changes
                            </button>
                        </div> --}}

                    </div>

                    {{-- 
                    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="deleteModalLabel">Delete</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                Are you sure to delete this?
                            </div>
                            <div class="modal-footer">
                                <form action="" method="POST" id="deleteModalForm">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger" id="deleteDayButton">Yes</button>
                                </form>
    
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                            </div>
                          </div>
                        </div>
                    </div>
                    --}}

                </div>


            </div>
        </div>
    </div>
@endsection
