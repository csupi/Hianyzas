@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>

                        <strong>Holy guacamole!</strong>
                        <p>{{ Session::get('success') }}</p>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="mb-3 mt-3">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <strong>Holy guacamole!</strong>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif

                <div class="card bg-white">
                    <div class="card-body">
                        <h4 class="card-title">Title</h4>
                        <div id="calendar"></div>
                    </div>
                </div>


                {{-- <div id="myModal" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Modal Title</h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <p>This is a simple Bootstrap modal. Click the "Cancel button", "cross icon" or "dark gray area" to close or hide the modal.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div> --}}

                <div class="modal fade" id="calendarEventModal" tabindex="-1" aria-labelledby="calendarEventModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="calendarEventModalLabel">Modal title</h5>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                          Choose an action.
                        </div>
                        <div class="modal-footer">
                            <form action="" method="GET" id="editForm">
                                @csrf
                                <button type="submit" class="btn btn-success" id="editButton">Edit</button>
                            </form>

                            <a type="button" class="btn btn-warning" id="attendancesButton" href="">Manage attendances</a>

                            @can('delete', $learndays->first())
                            <form action="" method="POST" id="deleteModalForm">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger" id="deleteDayButton">Delete</button>
                            </form>
                            @endcan

                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        


        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {
                timeZone: 'UTC',
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,timeGridDay'
                },
                editable: false,
                dayMaxEvents: true, // when too many events in a day, show the popover
                events: [
                    <?php
                    /* echo "{url: '". route('learndays.show', ['learnday' => $learndays[$i]]) ."', title: '" . $learndays[$i]->name . ' - ' . $learndays[$i]->course->name . "', start: '" . $learndays[$i]->date . "',end: '". $learndays[$i]->date ."',allDay: true},"; */
                    /* echo "{title: '" . $learndays[$i]->name . ' - ' . $learndays[$i]->course->name . "', start: '" . $learndays[$i]->date . "',end: '". $learndays[$i]->date ."',allDay: true},"; */
                    for ($i = 0; $i < count($learndays); $i++) {
                        echo "{id: '". $learndays[$i]->id ."', title: '" . $learndays[$i]->name . ' - ' . $learndays[$i]->course->name . "', start: '" . $learndays[$i]->date . "',end: '". $learndays[$i]->date ."',allDay: true},";
                    } 
                    ?>
                ],
                /* dateClick: function(info) {
                    alert('Date: ' + info.dateStr);
                    alert('Resource ID: ' + info.resource.id);
                }, */
                eventClick: function(info) {
                    /* $("#editForm").attr('action', "{{ route('learndays.edit', '') }}"+'/'+info.event.id) */
                    $("#editForm").attr("action", "{{route('learndays.edit', 'placeholder')}}".replace('placeholder', info.event.id) )
                    $("#attendancesButton").attr('href', "{{route('learndays.show', '')}}"+'/'+info.event.id)
                    $("#deleteModalForm").attr("action", "{{route('learndays.destroy', '')}}"+'/'+info.event.id)
                    $("#calendarEventModalLabel").html(info.event.title + ' - ' + info.event.id);
                    $("#calendarEventModal").modal('show');
                },

                /* eventDragStop: function( info )
                {
                    console.log(info.event.id, info.event.start, info.event.startStr);
                }, */
            });

            calendar.render();
        });
    </script>

@endsection
