@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card bg-white">
                    <div class="card-body">
                        <div class="d-flex mb-3">
                            <h4 class="card-title">{{ $learnday->name }} - {{ $learnday->course->name }} - {{ $learnday->date }}</h4>
                            @can('delete',$learnday)
                            <a name="deletedAttendance" id="deletedAttendance" class="btn btn-danger text-end" style="margin-left:auto !important" href="{{ route('learndays.delete_attendances', ['learnday' => $learnday]) }}" role="button">Delete Attendances</a>
                            @endcan
                        </div>

                        @if (Session::has('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="Close"></button>

                                <strong>Holy guacamole!</strong>
                                <p>{{ Session::get('success') }}</p>
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="mb-3 mt-3">
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                    <strong>Holy guacamole!</strong>

                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif

                        <div class="table-responsive">
                            <table class="table table-light">
                                <thead>
                                    <tr>
                                        <th scope="col">Name</th>
                                        <th scope="col" class="text-center">Status</th>
                                        <th scope="col" class="text-end">Modify status</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {{-- @php
                                     $diakok = array();   
                                     foreach ($learnday->course->students as $key => $value) {
                                        array_push($diakok, $value);
                                     }

                                    @endphp --}}

                                    <form action="{{ route('attendances.mass_update', $learnday) }}" method="POST" name="attendancesForm" id="attendancesForm">
                                        @foreach ($learnday->attendances as $item)
                                            <tr class="">
                                                <td scope="row">{{ $item->student->name }}</td>
                                                <td class="text-center">{{ $item->status }}</td>
                                                <td class="text-end">
                                                    @csrf
                                                    @method('PUT')
                                                    <div class="btn-group" role="group"
                                                        aria-label="Basic radio toggle button group">
                                                        <input type="radio" class="btn-check"
                                                            name="status_{{ $item->student->id }}"
                                                            id="status{{ $item->student->id }}1" autocomplete="off"
                                                            value="jelen" @checked(old('status_{{ $item->student->id }}', $item->status) == 'jelen')>
                                                        <label class="btn btn-outline-secondary"
                                                            for="status{{ $item->student->id }}1">Jelen</label>

                                                        <input type="radio" class="btn-check"
                                                            name="status_{{ $item->student->id }}"
                                                            id="status{{ $item->student->id }}2" autocomplete="off"
                                                            value="hiányzó" @checked(old('status_{{ $item->student->id }}', $item->status) == 'hiányzó')>
                                                        <label class="btn btn-outline-secondary"
                                                            for="status{{ $item->student->id }}2">Hiányzó</label>

                                                        <input type="radio" class="btn-check"
                                                            name="status_{{ $item->student->id }}"
                                                            id="status{{ $item->student->id }}3" autocomplete="off"
                                                            value="keresőképtelen" @checked(old('status_{{ $item->student->id }}', $item->status) == 'keresőképtelen')>
                                                        <label class="btn btn-outline-secondary"
                                                            for="status{{ $item->student->id }}3">Keresőképtelen</label>
                                                    </div>
                                                </td>
                                                {{-- <td class="text-end"> --}}
                                                {{-- <form action="{{ route('attendances.destroy', $item) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                    </form> --}}
                                                {{-- </td> --}}
                                            </tr>
                                        @endforeach
                                        {{-- <button type="submit" id="submitFormBtn" class="btn btn-primary d-none">
                                            Submit
                                        </button> --}}

                                    </form>
                                </tbody>
                            </table>
                        </div>

                        <div class="text-end">
                            <button type="button" class="btn btn-primary text-end" onclick="callFormSubmit();">
                                Save changes
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        let callFormSubmit = function() {
            document.getElementById("attendancesForm").submit();
        }
    </script>
@endsection
