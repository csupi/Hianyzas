@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>

                        <strong>Holy guacamole!</strong>
                        <p>{{ Session::get('success') }}</p>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="mb-3 mt-3">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <strong>Holy guacamole!</strong>

                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif

                <table id="studentsTable" class="display">
                    <thead>
                        <tr>
                            <th>Student name</th>
                            <th>Student birthdate</th>
                            <th>Student course</th>
                            <th>Actions</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($students as $item)
                            <tr>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->birthdate }}</td>
                                <td>{{ $item->course->name }}</td>
                                <td>
                                    <form action="{{ route('students.edit', $item) }}" method="GET">
                                        @csrf
                                        <button type="submit" class="btn btn-warning">Edit</button>
                                    </form>
                                </td>
                                <td>
                                    <form action="{{ route('students.destroy', $item) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                                {{-- <td>
                                    <form action="{{ route('students.show', $item) }}" method="GET">
                                        @csrf
                                        <button type="submit" class="btn btn-info">Show</button>
                                    </form>
                                </td> --}}
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        let table = new DataTable('#studentsTable', {
            responsive: true,
            ordering: true
        });
    </script>
@endsection
